# APS 2 - Desenvolvimento Web 1 #

**Professor:** Marcelo Soares Loutfi
**Período:** 2018.2
**Alunos:** Gabriel David Candez Rocha e Yure Alves Moutella

## Objetivos da APS ##

1. Tela de Login com validação dos campos usando feedback do tipo modal
2. Tela de Cadastro de Cliente (nome, sobrenome, data de nascimento, Endereço, 
   Cadastro do cartão de crédito). Validação de campos de formulário com JQuery 
   ou Javascript, usando feedback do tipo modal
3. Tela de Consulta de Passagem Aérea (ida, data da ida, volta, data da volta, 
   opção de marcar só ida
4. Tela da lista de resultados e escolha do vôo do cliente com destaque da linha
   após ser escolhido e botão de enviar
5. Tela de escolha de assento e cálculo do valor final
6. Tela de compra, escolha de cartão de crédito ou boleto bancário
7. Tela do mapa da origem e do destino selecionados pelo usuário
8. Tela de vídeo institucional (pode ser qualquer vídeo de alguma companhia a
   érea no youtube)

### Característica obrigatória ###

1. Testar no modo mobile **(deve ser responsivo)**